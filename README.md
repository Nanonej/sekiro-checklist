# Sekiro: Shadows Die Twice Checklist

To access the site [click here](https://nanonej.gitlab.io/sekiro-checklist/).

This checklist was created by adopting the source code from the [Dark Souls 3 Cheat Sheet](https://github.com/ZKjellberg/dark-souls-3-cheat-sheet) created by [Zachary Kjellberg](https://github.com/ZKjellberg).

## Contribution Guide

If you are interested in contributing to this guide, I welcome Pull Requests.

For some background on how the guide code is written, here is a sample item on the checklist:

```
<li data-id="playthrough_4_2" class="f_scale f_key"><span class="spoiler">Left to the bridge behind around a rock, you will find</span> 1x <a href="https://sekiroshadowsdietwice.wiki.fextralife.com/Treasure+Carp+Scale">Treasure Carp Scale</a> on the ground</li>
```

The **data-id** is a unique ID used to store the user's progress. For example, ***playthrough_3_8*** is the 8th task in zone 3. New data-ids must be used in ascending order, but you can place the new entries anywhere within a zone.

The **class="f_scale f_key"** field is used for the filtering system. This task provides the user with a Treasure Carp Scale, so we use **f_scale** and it's also a key item so we use **f_key** too. The full list of filter classes is:

| Class      | Description |
|---         |--- |
| f_boss     | Boss & Mini-Boss fights |
| f_npc      | NPC side quests |
| f_remnant  | Remnants |
| f_gourd    | Gourd Seeds |
| f_bead     | Prayer Beads |
| f_esoteric | Esoteric Texts and Skill Texts |
| f_tool     | Prosthetic Tools |
| f_droplet  | Dragon's Blood Droplets |
| f_material | Prosthetic upgrade materials |
| f_scale    | Treasure Carp Scale |
| f_key      | Other Key Items |
| f_misc     | *any other items* |

If none of these filter classes match, use **class="f_none"**.

<!-- In addition to the filter classes, there is a second type of classes used to control the visibility of entries based on which playthrough the user is on:

| Class  | Description |
|---     |--- |
| h_ng+  | items hidden on NG+ and beyond, e.g., Ashen Estus Flask |
| s_ng+  | items shown on NG+ and beyond, e.g., +1 rings |
| s_ng++ | items shown on NG++ and beyond, e.g., +2 rings | -->

Finally **span class="spoiler"** is used to hide sensitive informations to let the player search the item by itself without spoil.
